import React, { Component } from "react";
 
class Contact extends Component {
  render() {
    return (
      <div>
        <h2>Contact</h2>
        <p>The easiest thing to do is post on
        our <a href="https://reactjs.org/">forums</a>.
        </p>
      </div>
    );
  }
}
 
export default Contact;